# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Purchase Product Rule',
    'name_de_DE': 'Einkauf Artikel Regel',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account rules for products on purchase
    - Replaces the fixed accounts on products by rules for accounts on purchase
    ''',
    'description_de_DE': '''Kontenregeln für Artikel im Einkauf
    - Ersetzt die fixierten Konten in den Artikeln durch Kontenregeln
      im Einkauf.
    ''',
    'depends': [
        'account_product_rule',
        'account_invoice_product_rule',
        'purchase',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
