#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class PurchaseLine(ModelSQL, ModelView):
    _name = 'purchase.line'

    def get_invoice_line(self, line):
        pool = Pool()
        account_rule_obj = pool.get('account.account.rule')

        res, = super(PurchaseLine, self).get_invoice_line(line)
        tax_ids = [x.id for x in line.taxes]
        res['taxes'] = [('set', tax_ids)]
        res['account'] = account_rule_obj.get_account(
            'expense', line.purchase.party, line.purchase.purchase_date,
            line.product, tax_ids)
        return [res]

PurchaseLine()


class Template(ModelSQL, ModelView):
    _name = "product.template"

    def __init__(self):
        super(Template, self).__init__()
        self.account_expense = copy.copy(self.account_expense)
        self.account_expense.states = copy.copy(self.account_expense.states)
        self.account_expense.states['required'] = False
        self._reset_columns()


Template()
